const { Given, When, Then } = require('@cucumber/cucumber');
const assert = require('assert');
const ppt = require('./hooks');

let productIds = [];

Given('Bob is on the listing page', async () => {
    //direct to modanisa homepage
    await ppt.page.goto('https://www.modanisa.com', { waitUntil: 'domcontentloaded' });

    //check the current language and shipping
    const preferredLang = 'Türkçe - TL'

    const langBarInnerHTML = await ppt.page.evaluate(() => {
        const langBar = document.querySelector('#lang'); // Replace '.lang-bar' with the selector of your lang bar element
        return langBar.innerHTML;
    });

    const langText = langBarInnerHTML.replace(/<\/?[^>]+(>|$)/g, '');
    console.log(langText)

    if (langText !== preferredLang) {
        await ppt.page.click('#lang')
        await ppt.page.waitForSelector('#ship-country-form > span > span > a');
        await ppt.page.click('#ship-country-form > span > span > a');
        await ppt.page.waitForTimeout(2000)
        await ppt.page.click('.combobox-item')
        await ppt.page.waitForTimeout(2000)
        await ppt.page.keyboard.press('Enter')
        await ppt.page.waitForNavigation()
    }
    
    //click the banner
    bannerUrl = await ppt.page.evaluate(() => {
        let banner = document.querySelector('[data-testid="desktop_header_slot1_tr-0_link"]');
        return banner ? banner.getAttribute('href') : null;    //return href of the banner, if not available then return null
    })
    await ppt.page.click('[data-testid="desktop_header_slot1_tr-0_link"]');
    await ppt.page.waitForNavigation({ waitUntil: 'domcontentloaded' })
    
});

When('Bob selects a product', async () => {
    let productId = await ppt.page.evaluate(() => {
        let product = document.querySelector('[data-testid="listing-product"]:nth-child(12)');
        return product ? product.getAttribute('data-product-id') : null;  //take the product id
    })
    productIds.push(productId)  //place it in an array
    await ppt.page.waitForSelector('[data-testid="listing-product"]:nth-child(12)', { waitUntil: 'domcontentloaded' })
    await ppt.page.click('[data-testid="listing-product"]:nth-child(12)');
    console.log('\nclicked to product', productId)
});

Then('Bob should see the product detail page', async () => {
    await ppt.page.waitForSelector('#productData')
    const currentProductId = await ppt.page.evaluate(() => {
        let product = document.querySelector('#productData')
        return product ? product.getAttribute('data-product-id') : null;    //take the directed products' id
    })
    productIds.push(currentProductId) //place it in the same array we've created before

    try {
        assert.strictEqual(productIds[0], productIds[1])   //check if they are the same to verify the directed product
    } catch (error) {
        throw new Error('Bob got misdirected into a wrong product!:', productIds[1])  //if the id's not equal, throw an error with the directed product id
    }
});

module.exports = productIds