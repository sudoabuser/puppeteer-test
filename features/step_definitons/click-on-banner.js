const { Given, When, Then, setDefaultTimeout } = require('@cucumber/cucumber');
const assert = require('assert');
const ppt = require('./hooks');
setDefaultTimeout(20000);

Given('Bob is on the homepage', async () => {
    //direct to modanisa homepage
    await ppt.page.goto('https://www.modanisa.com', { waitUntil: 'domcontentloaded' });

    //check the current language and shipping
    const preferredLang = 'Türkçe - TL'

    const langBarInnerHTML = await ppt.page.evaluate(() => {
        const langBar = document.querySelector('#lang'); // Replace '.lang-bar' with the selector of your lang bar element
        return langBar.innerHTML;
    });

    const langText = langBarInnerHTML.replace(/<\/?[^>]+(>|$)/g, '');
    console.log(langText)

    if (langText !== preferredLang) {
        await ppt.page.click('#lang')
        await ppt.page.waitForSelector('#ship-country-form > span > span > a');
        await ppt.page.click('#ship-country-form > span > span > a');
        await ppt.page.waitForTimeout(2000)
        await ppt.page.click('.combobox-item')
        await ppt.page.waitForTimeout(2000)
        await ppt.page.keyboard.press('Enter')
        await ppt.page.waitForNavigation()
    }

    //check if the title of the logo is modanisa
    const logoTitle = await ppt.page.evaluate(() => {
        let logo = document.querySelector("#header > div.wrapper > div > a.logo");
        return logo ? logo.getAttribute('title') : null;
    });

    try {
        assert.strictEqual(logoTitle, "Modanisa")
    } catch (error) {
        throw new Error("Bob's lost!!.. He's floating the web.", error)
    }

});

let bannerUrl;    //to use the url in multiple functions.

When('Bob clicks on a banner', async () => {
    bannerUrl = await ppt.page.evaluate(() => {
        let banner = document.querySelector('[data-testid="desktop_header_slot1_tr-0_link"]');
        return banner ? banner.getAttribute('href') : null;    //return href of the banner, if not available then return null
    })
    await ppt.page.click('[data-testid="desktop_header_slot1_tr-0_link"]');
});

Then('Bob should be redirected to the associated listing page', async () => {
    await ppt.page.waitForNavigation({ waitUntil: 'domcontentloaded' })
    const directedUrl = await ppt.page.url();     //get the url of the current page

    console.log('\nBanner href:', bannerUrl);
    console.log('Directed URL:', directedUrl);

    try {
        assert.strictEqual(bannerUrl, directedUrl);
    } catch (error) {
        throw new Error("Bob accidentally clicked on a wrong banner! :(");
    }
});

